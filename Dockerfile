FROM registry.gitlab.com/registry/x2go-mate:master

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EB3E94ADBE1229CF \
    && add-apt-repository -y "deb https://packages.microsoft.com/repos/vscode stable main" \
    && apt update \
    && apt -y install code